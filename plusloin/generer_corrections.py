import os
import fnmatch
import sys
import re
dir = 'terminale' # directory inside the same folder of the script
filepattern = "*_corr.tex"
files = [f for f in os.listdir(sys.path[0]+"/"+dir) if fnmatch.fnmatch(f, filepattern)]
files.sort()
for f in files:
  print ("\n\n\n===============\n"+ f)
  # x = re.findall("(\d+)", f) # on récupère le numéro de la seance
  # target = "\def\SeanceCount{{{scount}}}\def\ExoCount{{{ecount}}}\includeonly{{{filename}}}\input{{{main}}}".format(filename=dir+"/"+f, main=sys.path[0]+"/plusloin_terminale",scount=x[0],ecount=x[1])
  target = "\includeonly{{{filename}}}\input{{{main}}}".format(filename=dir+"/"+f, main=sys.path[0]+"/plusloin_terminale")
  command = "lualatex -shell-escape -synctex=1 -interaction=nonstopmode -file-line-error -pdf -jobname={outputname} \"{t}\"".format(outputname=f.replace(".tex",""),t=target)
  print("target: {}".format(target))
  print("command: {}".format(command))
  print("==============\n")
  os.system(command)