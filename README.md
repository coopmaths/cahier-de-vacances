# Cahier de Vacances - Coopmaths

## Fonctionnement   

Pour créer un cahier de vacances, il suffit de créer un fichier `mon_cahier.tex` utilisant la
classe `cdv` comme suit.

```latex
\documentclass{cdv}
```

On ajoute chaque séance dans un environnement `seance` prévu à cet effet. Tous les fichiers
correspondant à une séance sont stockés dans le dossier `seances` du dépôt dans le niveau
correspondant.

Si par exemple je souhaite intégrer à mon cahier la séance `seance1` en Première Spé :

```latex
\begin{seance}
    \input{seances/premiere_spe/seance1.tex}
\end{seance}
```
Un fichier pour une séance est dans un environnement `Maquette` de type *Parcours* (voir le package [ProfMaquette] (http://mirrors.ctan.org/macros/latex/contrib/profmaquette/doc/ProfMaquette-doc.pdf) de Christophe Poulain)

```latex
\begin{Maquette}[Parcours,CorrigeFin]{Theme={}, Date={}, Niveau={}}
  ...
\end{Maquette}
```


## Environnement `seance`

Des options sont à renseigner pour cet environnement :

| Option      | Valeur par défaut      | Fonction                                                                 |
| ----------- | ---------------------- | ------------------------------------------------------------------------ |
`nbex` | 4 | Nombre d'exercices dans la séance (à automatiser dans le futur) |
`enigme` | 1 | Numéro de l'énigme associée à la séance (pour la valeur `1` doit correspondre au chemin `enigmes/enigme1`) |
`qrcodeverif` | 1 | URL de la série MAthALÉA pour vérification |

Exemple d'utilisation :

```latex
\begin{seance}[nbex=6,enigme=1,%
  qrcodeverif={https://coopmaths.fr/alea/?uuid=b51ec&id=2N30-2&n=2&d=10&s=2&s2=true&s3=true&s4=false&i=1&cd=1&uuid=29919&id=2N30-3&n=1&d=10&s=1&s2=false&s3=true&s4=3&i=1&cd=1&uuid=98658&id=2N40-2&n=1&d=10&i=1&cd=1&uuid=60cc5&id=2N40-4&n=2&d=10&i=1&cd=1&uuid=ae913&id=2S10-1&n=1&d=10&s=4&i=1&cd=1&uuid=612a5&id=2S10-2&n=2&d=10&s=4&i=1&cd=1&v=eleve&es=1111001&title=Je+teste+mes+connaissances}]
  \input{seances/premiere_spe/seance1.tex}
\end{seance}
```

## Environnement `seancestart`

Cet environnement permet de positionner un cadre en guise de préambule où lister les points abordés dans la séance et les inclure un QRCode pour se tester sur les prérequis.

| Option      | Valeur par défaut      | Fonction                                                                 |
| ----------- | ---------------------- | ------------------------------------------------------------------------ |
`qrcode` | 1 | URL de la série MAthALÉA pour se tester |

Exemple d'utilisation :

```latex
  % Préambule séance
  \begin{seancestart}[qrcode={https://coopmaths.fr/alea/?uuid=12514&id=can4L04&alea=wcPR&uuid=97664&id=can4L07&alea=orrq&uuid=1853b&id=can3C04&alea=EtpH&uuid=59365&id=can3C11&alea=FV3o&uuid=864ba&id=can3C15&alea=Rh18&uuid=cb6b3&id=can3L03&alea=Tb3X&uuid=d6cd2&id=can3L10&alea=fi3C&uuid=f6f76&id=can2N01&alea=IxoM&uuid=ed8da&id=can5N03&alea=XzF6&uuid=22c4c&id=can4C03&alea=9x0k&v=can&es=011100&canD=5&canT=2024&canSA=1&canSM=gathered&canI=1}]
    \begin{itemize}
      \item \textbf{Calcul numérique :} puissances
      \item \textbf{Fonctions :} généralités
      \item \textbf{Géométrie repérée:} milieux
    \end{itemize}
  \end{seancestart}
  ```

## Environnement `cdvds`

Pour ajouter un devoir surveillé au cahier de vacances, on dispose d'un environnement `cdvds`. Avec la clé `theme` on précise un sous-titre.

```latex
\begin{cdvds}[theme={Synthèse de calcul littéral}]
  \input{DS/premiere_spe/DS1.tex}
\end{cdvds}
```

Le fichier contenant le code du devoir surveillé doit être inclus dans un environnement `Maquette` de type `Fiche` :

```latex
\begin{Maquette}[Fiche]{Theme={}, Date={}, Niveau={}}
 ...
\end{Maquette}

```

On pourra ajouter une option à l'environnement des exercices pour mettre la couleur du liseré en orange ainsi :

```latex
\begin{exercice}[Cadre=OrangeCoopmaths]
 ...
\end{exercice}
```


## Options de la classe `cdv`

Les options de la classe `cdv` se déclarent entre crochet après la commande `\documentclass` :

```latex
\documentclass[<mon_option>=<ma_valeur>]{cdv}
```

Les options disponibles sont :

| Option      | Valeur par défaut      | Fonction                                                                 |
| ----------- | ---------------------- | ------------------------------------------------------------------------ |
| `niveau`    | `Tle Spé`              | Indiquer le niveau (en couverture et dans les chapeaux de chaque séance) |
| `mainfont`  | `Source Sans Pro`      | Indiquer la fonte principale                                             |
| `monofont`  | `Source Code Pro`      | Indiquer la fonte à largeur fixe                                         |
| `mathfont`  | `GFS Neohellenic Math` | Indiquer la fonte pour les expressions mathématiques                     |
| `titlefont` | `Jellee`               | Indiquer la fonte pour les titres des séances                            |
| `twocolumnstoc` | `true`               | Deux colonnes pour la table des matières                           |
| `stapled` | `false`               | Marge intérieure supérieure de 1cm pour agrafage                           |

## Bonus

### Table des matières

On peut insérer une tableau des matières avec la commande traditionnelle `\tableofcontents`. Des paramétrages décrits dans la classe assurent son intégration stylistique.

#### Entrées différentes du corps de texte

Penser à bien renseigner les titres alternatifs des sections si besoin comme suit :

```latex
\section[Calcul numérique~: fractions]{Calcul numérique}
```

Dans cet exemple, on aura :

- **Calcul numérique** dans le corps du texte ;
- **Calcul numérique~: fractions** dans la table des matières.

#### Deux colonnes

Pour avoir deux colonnes dans la table des matières, il suffit de mentionner l'option `twocolumnstoc` pour la classe `cdv`.

```latex
\documentclass[niveau={Première Spé},twocolumnstoc]{cdv}
```

### Un environnement avec chapeau paramétrable

En plus de l'environnement `seance`, le package `cdv` met à votre disposition un environnement avec le même type de chapeau que celui de l'environnement `seance` mais avec quelques éléments paramétrables.

```latex
\begin{infoswithheader}[title={Des News},color=white,size=36pt]
  Des informations de qualité ici.
\end{infoswithheader}
```

| Option | Valeur par défaut | Fonction |
|---|---|---|
`title` | `Informations` | contenu du titre |
`color` | `white` | couleur du titre |
`size` | `30pt` | taille du titre |

### Corrigé des devoirs surveillés

Pour indiquer l'accès au corrigé d'un devoir surveillé, une seule commande `\CorrigeDS` prenant comme argument le lien complet du stockage du devoir.

```latex
\CorrigeDS{https://coopmaths.fr/www/pdf/cdv1re/ds1_corr.pdf}
```


## Recommandations

### Erreurs en rapport avec les SVG

Lors de la compilation, si vous avez des erreurs en rapport avec les SVG, c'est que le compilateur n'arrive pas à trouver [Inkscape](https://inkscape.org/release/). Vous pouvez tenter de l'installer. Plus simplement, vous pouvez aussi télécharger le dossier [svg-inkscape.zip](https://forge.apps.education.fr/coopmaths/cahier-de-vacances/-/raw/main/svg-inkscape.zip?ref_type=heads) et le décompresser à la racine du projet.

### Localisation de la classe et du package

Pour que cela fonctionne, le compilateur doit trouver `cdv.cls` et `cdv.sty`. Si ces deux fichiers
sont situés à côté du fichier maître, cela fonctionnera. Cependant, il est préférable de localiser
la classe `cdv.cls` et le package `cdv.sty` dans `~/texmf/tex/latex/` (ou tout autre chemin pour la
branche contenant les packages/classes personnelles).

On pourra réfléchir à terme à entretenir les classes/packages et les sources dans deux dépôts
différents, ce qui serait idéal.

### Fontes

Pour être utilisée, une fonte doit être installée ;-)

## Participer

Si vous voulez participer, vous pouvez envoyer un mail à contact@coopmaths.fr
